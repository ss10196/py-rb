from pychart import *
def y_min(data, n_col):
	min = data[0][n_col]
	for i in range(len(data)):
		if(data[i][n_col] < min):
			min = data[i][n_col]
	return min
			
def pychart_offical_demo():
	import sys,os
	print(os.getcwd())
	f=os.open("test.pdf",os.O_CREAT|os.O_WRONLY)
	os.dup2(f,os.sys.stdout.fileno())

	theme.get_options()

	data = [(10, 20, 30), (20, 65, 33),
		(30, 55, 30), (40, 45, 51),
		(50, 25, 27), (60, 75, 30),
		(70, 80, 42), (80, 62, 32),
		(90, 42, 39), (100, 32, 39)]


	xaxis = axis.X(format="/a-60/hL%d", tic_interval = 20, label="Stuff")
	yaxis = axis.Y(tic_interval = 20, label="Value")


	ar = area.T(x_axis=xaxis, y_axis=yaxis, y_range=(0,None))

	plot = line_plot.T(label="foo", data=data, ycol=1, tick_mark=tick_mark.star)
	plot2 = line_plot.T(label="bar", data=data, ycol=2, tick_mark=tick_mark.square)

	ar.add_plot(plot, plot2)

	ar.draw()
def pychart_rb(data,pdf_name,n_col):
	min_y = y_min(data, n_col)
	import sys,os
	file = open(pdf_name,"w")
	os.dup2(file.fileno(),os.sys.stdout.fileno())
	

	theme.get_options()
	xaxis = axis.X(format="/a-60/hL%d", tic_interval = 1, label="period")
	yaxis = axis.Y(tic_interval = 1, label="Value")
	ar = area.T(x_axis=xaxis, y_axis=yaxis, y_range=(min_y,None),x_range=(data[0][0],None))

	plot = line_plot.T(label=str(n_col), data=data, ycol=n_col, tick_mark=tick_mark.star)
	ar.add_plot(plot)	
	ar.draw()


#data_me = [(2013047,1,3,5,7,9,6,4),(2013048,2,4,6,8,0,9,13)]
#data_me = [(51, 3, 7, 13, 18, 22, 25, 3), (52, 8, 12, 15, 19, 28, 29, 2), (53, 6, 7, 14, 21, 22, 24, 13)]

#b = pychart_rb(data_me,"data-me.pdf")
