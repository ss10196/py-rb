def search_pdf():
	import os
	tmp = []
	for i,j,k in os.walk('.'):
		for m in k:
			file_name = os.path.join(i,m)		
			if(".pdf" in file_name):
				tmp.append(file_name)
	return tmp

def modify_pdf():
	file_names = search_pdf()
	for file_name in file_names:
		#print(file_name)
		flag = False
		with open(file_name,"r") as f:
			line = f.readline()
			while(line):
				line = f.readline()
				if("%!PS" in line):
					flag = True
					with open("temp","w") as f_temp:
						f_temp.write(line)	
				if(flag == True):
					with open("temp","a") as f_temp:
						f_temp.write(line)	
		import os
		if(flag == True):
			cmd = "cp temp %s -f" %file_name
			os.system(cmd)
#modify_pdf()
__all__=[]
__all__.append("search_pdf")
__all__.append("modify_pdf")
