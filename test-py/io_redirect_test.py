import os
import sys
import threading

def test(i):
	print("in thread %s redirect before" %i)
	import os
	file_name = "io_"+str(i)+".txt"
	f = open(file_name,"w")
	os.sys.stdout=f	
	print("in thread %s redirect after" %i)
th1 = threading.Thread(target=test,args=(1,))
th2 = threading.Thread(target=test,args=(2,))
print("main begin")
th1.start()
sys.stdout = sys.__stdout__
print("th1 start after")
th2.start()
th1.join()
th2.join()
print("main end")
